# Engrain Units WordPress Plugin

@author: Peter Toth - peter@petertoth.email

Here's my solution of the Back-end Developer Assesssment

- Create a simple WordPress plugin with the following functions:
- The plugin should register a custom post type `unit`.
- The plugin should provide an admin page within the WordPress CMS.
- The plugin's admin page should include a custom button to trigger an API call.
- On click of the button the plugin should consume JSON API and create `unit` records.
- The admin page for `unit` posts should include a custom column displaying the `floor_plan_id` field included on the unit record.
- The post title for `unit` post's title should be set to the unit number field.
- The edit page for the unit admin post should display the required custom fields.

### Unit Specification:
`Post title -> unit_number`

### Custom fields required:
- asset_id
- building_id
- floor_id
- floor_plan_id
- area



# Shortcode
`[engrain-units]` shortcode displays a list of the unit posts broken into two sections, units with an area greater than 1 and units with an area of 1

# Install
Just download and copy the code to `wp-content/plugins/engrain-units` folder

# Dependecies
    None

# PHP version 
    >=8.0