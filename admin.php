<?php


class EngrainUnitsAdmin{

    static function init(){
        add_action( 'admin_menu', ['EngrainUnitsAdmin', 'create_admin_menu'] );
        add_action( 'admin_footer', ['EngrainUnitsAdmin', 'fetch_units_js'] );
        add_action( 'wp_ajax_fetch_units', ['EngrainUnitsAdmin', 'fetch_units'] );
        add_action( 'save_post_unit', ['EngrainUnitsAdmin', 'update_custom_fields_values'], 20, 3 );
    }

    static function create_admin_menu() {
        add_menu_page( 'Fetch Units', 'Fetch Units', 'manage_options', 'engrain-units/admin.php', ['EngrainUnitsAdmin','create_page'], 'dashicons-admin-multisite', 6  );
    }

    static function create_page(){
        $user = wp_get_current_user();
        echo '
        <h1>Hello '.$user->user_nicename.', let\'s fetch those items!</h1>
        <div><button class="button button-primary" type="button" id="fetch-units-btn">Fetch Units</button></div>
        <span id="message"></span>
        ';
    }

    static function fetch_units_js() { ?>
        <script type="text/javascript" >
            jQuery(document).ready(function($) {
                jQuery( "#fetch-units-btn" ).on('click', function() {
                    $(this).text('Fetching units, please wait ...')
                    jQuery.post(ajaxurl, {
                        'action': 'fetch_units',
                    }, function(response) {
                        jQuery( "#fetch-units-btn" ).text('Fetch Units').attr('disabled','disabled');
                        jQuery( "#message" ).html('Fetching is done, you can find the results <a href="/wp-admin/edit.php?post_type=unit">here</a>');
                    });
                });
            });
        </script> <?php
    }

    static function fetch_units(){
        $response = wp_remote_get( 'https://api.sightmap.com/v1/assets/1273/multifamily/units?per-page=250', ['headers' => ['API-key'=>'7d64ca3869544c469c3e7a586921ba37', 'Content-Type' => 'application/json']]);
        try {
            $json = json_decode( $response['body'] );
        } catch ( Exception $ex ) {
            $json = null;
        }

        if(!empty($json->data)){
            foreach ($json->data as $unit){
                self::create_unit((array) $unit);
            }
        }

        wp_die();
    }

    static function create_unit(array $data){
        $user_id = get_current_user_id();
        $meta_input = [];
        foreach(CustomFields::$custom_fields as $name=>$title){
            $meta_input['engrain-units-'.$name] = $data[$name];
        }

        wp_insert_post([
            'post_title'    => wp_strip_all_tags( $data['unit_number'] ),
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => $user_id,
            'post_type'     => 'unit',
            'meta_input'    => $meta_input
        ]);

    }

    static function update_custom_fields_values( $post_id ) {
        foreach(CustomFields::$custom_fields as $name=>$title) {
            if (array_key_exists('engrain-units-'.$name, $_POST)) {
                update_post_meta(
                    $post_id,
                    'engrain-units-'.$name,
                    $_POST['engrain-units-'.$name]
                );
            }
        }
    }
}

add_action( 'plugins_loaded', ['EngrainUnitsAdmin', 'init']);

