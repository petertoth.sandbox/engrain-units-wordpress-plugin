<?php
/**
 * @package Engrain Units
 */
/*
Plugin Name: Engrain Units
Description: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
Version: 0.0.1
Requires PHP: 8.0
Author: Peter Toth
Author URI: https://www.linkedin.com/in/peterftoth/
Text Domain: engrain-units
*/

if ( !function_exists( 'add_action' ) ) {
    echo 'Critical error';
    exit;
}

define( '__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once( __PLUGIN_DIR . 'admin.php' );

class CustomFields
{
    static $custom_fields = [
        'asset_id' => 'Asset ID',
        'building_id' => 'Building ID',
        'floor_id' => 'Floor ID',
        'floor_plan_id' => 'Floor Plan ID',
        'area' => 'Area',
    ];
}

class EngrainUnits{
    static function init(){
        wp_enqueue_style( 'style', plugin_dir_url(__FILE__).'style.css' );
        add_action( 'init', ['EngrainUnits','create_post_type'] );
        add_action( 'init', ['EngrainUnits', 'remove_content_editor']);

        add_action( 'add_meta_boxes', ['EngrainUnits', 'add_custom_fields'] );
        add_filter( 'manage_unit_posts_columns', ['EngrainUnits','init_custom_admin_columns'] );
        add_action( 'manage_unit_posts_custom_column', ['EngrainUnits', 'set_custom_admin_columns'], 10, 2 );
        wp_enqueue_style( 'style', get_stylesheet_uri() );
        add_shortcode('engrain-units', ['EngrainUnits', 'list_units']);

    }

    static function create_post_type() {
        register_post_type( 'unit',
            array(
                'labels' => array(
                    'name' => __( 'Units' ),
                    'singular_name' => __( 'Unit' )
                ),
                'public' => true,
            )
        );
    }

    static function add_custom_fields(){
        foreach(CustomFields::$custom_fields as $name=>$title){
            self::add_custom_field($name, $title);
        }
    }

    static function add_custom_field($name, $title){
        add_meta_box(
            'engrain-units-'.$name,
            $title,
            function( $post ) use ($name, $title) {
                $value = get_post_meta( $post->ID, 'engrain-units-'.$name, true );
                echo'
                <input type="number" name="engrain-units-'.$name.'" id="engrain-units-'.$name.'" value="'.$value.'" placeholder="'.$title.'">
            ';
            },
            'unit'
        );
    }

    static function init_custom_admin_columns($columns) {
        $columns['engrain-units-floor_plan_id'] = 'Floor Plan ID';
        return $columns;
    }


    static function set_custom_admin_columns( $column_name, $post_id ) {
        if( !empty($column_name) && !empty($post_id) ) {
            echo get_post_meta( $post_id, $column_name, true );
        }
    }

    static function remove_content_editor() {
        remove_post_type_support('unit', 'editor' );
    }

    static function list_units(){
        $posts = self::get_units();
        $units = ['areaEq1'=>'', 'areaGt1'=>''];
        foreach($posts as $post){
            $area = get_post_meta( $post->ID, 'engrain-units-area', true );
            if((int)$area  === 1){
                $units['areaEq1'] .= '<tr><td>'.$post->ID.'</td><td>'.$post->post_title.'</td></tr>';
            }
            if((int) $area > 1){
                $units['areaGt1'] .= '<tr><td>'.$post->ID.'</td><td>'.$post->post_title.'</td></tr>';
            }
        }

        $html = '
            <h2>Units with Area = 1</h2>
            <table class="table table-stripped">
                <thead>
                <tr><th>ID</th><th>Title</th></tr>
                </thead>
                <tbody>'.$units['areaEq1'].'</tbody>
            </table>
        ';

        $html .= '
            <h2>Units with Area > 1</h2>
            <table class="table table-stripped">
                <thead>
                <tr><th>ID</th><th>Title</th></tr>
                </thead>
                <tbody>'.$units['areaGt1'].'</tbody>
            </table>
        ';

        return $html;
    }

    static function get_units(){
        $args = array(
            'post_type' => 'unit',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC',
        );

        return get_posts($args);
    }

}

EngrainUnits::init();



